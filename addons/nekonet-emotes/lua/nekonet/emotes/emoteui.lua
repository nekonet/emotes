local emoteui = {}

---@class EmoteUI
local EmoteUI = {}
EmoteUI.__index = EmoteUI

---@param emotes table
---@return EmoteUI
function emoteui.New(emotes)
    local e = {}
    setmetatable(e, EmoteUI)

    e.emotes = emotes

    return e
end

---@param self EmoteUI
local function init(self)
    self.frame = vgui.Create("DFrame")
    self.frame:SetDeleteOnClose(false)
    self.frame:SetTitle("Emote List")
    self.frame:SetSize(ScrW()/3, ScrH()/2)

    self.listView = vgui.Create("DListView", self.frame)
    self.listView:SetPos(30, 50)
    self.listView:SetSize(self.frame:GetWide()-60, self.frame:GetTall() - 60)
    self.listView:SetMultiSelect(false)
    self.listView:AddColumn("Trigger Text")
    self.listView:AddColumn("Sound File")

    -- TODO: multi file support
    for k,v in pairs(self.emotes) do
        self.listView:AddLine(v.Trigger, v.Files[1])
    end

    self.listView.OnClickLine = function(parent, line, isSelected)
        RunConsoleCommand("play", line:GetValue(2))
    end
end

function EmoteUI:Open()
    if self.frame == nil then
        init(self)
    end

    self.frame:Center()
    self.frame:SetVisible(true)
    self.frame:MakePopup()
end

-- Install hooks on player chat for the /emotes command.
function EmoteUI:Install()
    hook.Add( "OnPlayerChat", "NekoEmotesOpen", function(ply, msg, isTeamChat, isDead)
        if ( ply ~= LocalPlayer() ) then
            -- hide other people's commands
            local msg = string.lower( msg ) -- make the string lower case

            if msg == "/emotes" then
                return true
            end
            return false
        end

        local msg = string.lower( msg ) -- make the string lower case

        if ( msg == "/emotes" ) then
            self:Open()
            -- do not print
            return true
        end
    end)
end

function EmoteUI:Uninstall()
    hook.Remove("OnPlayerChat", "NekoEmotesOpen")
end

return emoteui