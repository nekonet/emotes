local emotes = {}

---@class Emote
local Emote = {}
Emote.__index = Emote

--- New returns a new Emote.
---@param trigger string
---@param files table
---@param shouldHideInChat boolean
---@return Emote
function emotes.New(trigger, files, shouldHideInChat)
    local emote = {
        Trigger = trigger,
        Files = files,
        ShouldHideInChat = shouldHideInChat
    }
    setmetatable(emote, Emote)

    return emote
end

return emotes