local client = {}

local list = include("nekonet/emotes/list.lua")

--- @class Client
local Client = {}
Client.__index = Client

-- forward declarations
local play

-- holds sounds so the garbage collector doesn't kill them
local LoadedSounds = {}

--- New returns a new Client that listens to chat messages for trigger phrases
--- and plays a sound effect originating at the player.
---@return Client
function client.New()
    -- load emotes
    local c = {}
    setmetatable(c, Client)

    -- load emotes into the table
    -- map[string]Emote for O(1) access by trigger phrase
    c.emotes = {}

    for _, emote in pairs(list) do
        c.emotes[emote.Trigger] = emote
    end

    return c
end

--- Start installs hooks and begins turning chat messages into emotes.
function Client:Start()
    hook.Add("OnPlayerChat", "NekoEmotes", function(ply, message, isTeamChat, isDead)
        local msg = string.Trim(string.lower(message))

        if self.emotes[msg] == nil then
            -- not an emote, print to chat
            return nil
        end

        play(ply, self.emotes[msg].Files, 75, false)

        if self.emotes[msg].ShouldHideInChat then
            -- intercept this message and do not print to chat
            return true
        end
    end)
end

--- Stop uninstalls hooks and ceases turning chat messages into emotes.
function Client:Stop()
    hook.Remove("OnPlayerChat", "NekoEmotes")
end

--- play plays a sound effect originating at the given player.
--- @param ply player
--- @param files table
--- @param pitch number
--- @param hasVoiceChanger boolean
play = function(ply, files, pitch, hasVoiceChanger)
    if not IsValid(ply) then
        return
    end

    local file = files[math.random(#files)]

    -- wiki suggests caching it
    if not LoadedSounds[ply:SteamID()..file] then
        local emoteSound = CreateSound(ply, file)
        emoteSound:SetSoundLevel(65)
        LoadedSounds[ply:SteamID()..file] = emoteSound
    end

    local sound = LoadedSounds[ply:SteamID()..file]
    sound:Stop()
    sound:Play()
end

return client