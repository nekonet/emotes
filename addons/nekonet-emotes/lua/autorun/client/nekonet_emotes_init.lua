-- modules
local emotes = include("nekonet/emotes/client.lua")
local emoteui = include("nekonet/emotes/emoteui.lua")
local emotelist = include("nekonet/emotes/list.lua")

--- Init
local emoteClient = emotes.New()
emoteClient:Start()

local emoteUI = emoteui.New(emotelist)
emoteUI:Install()

print("NekoNet Emotes Client Loaded")
print("Say /emotes to bring up a list of sound effects.")